## 分支结构

### if

##### 第一种格式

格式：
if (条件) {
    语句体;	
}
… 

首先判断条件表达式的结果，如果为true执行语句体，为 false 就不执行语句体。
if 语句中，如果大括号控制的只有一行代码，则大括号可以省略不写。

##### 第二种格式

格式：
if (条件) {
    语句体1;	
} else {
    语句体2;	
}
...

首先判断条件表达式的结果，如果为true执行语句体1，为 false 就执行语句体2。

##### 第三种格式

格式：
if (条件1) {
    语句体1;	
} else if (条件2) {
    语句体2;	
} else if (条件3) {
    语句体4;	
} 
. . .
else {
    语句体n+1;
}

1.先判断条件1的值，如果为true则执行语句体1，分支结束；如果为false则判断条件2的值
2.如果值为true就执行语句体2，分支结束；如果为false则判断条件3的值
3....
4.如果没有任何条件为true，就执行else分支的语句体n+1。

### switch

switch(表达式){
   case 值1:
     执行代码...;
     break;
   case 值2:
     执行代码...;
     break;

  … 
   case 值n-1:
     执行代码...;
     break;
   default:
     执行代码n;
 }

先执行表达式的值，拿着这个值去与case后的值进行匹配。
匹配哪个case的值为true就执行哪个case，遇到break就跳出switch分支。
如果case后的值都不匹配则执行default代码。

## 循环结构

### for循环

格式：

for (初始化语句; 循环条件; 迭代语句)  {
  循环体语句(重复执行的代码);

}

### while循环

初始化语句;

while (循环条件) {
   循环体语句(被重复执行的代码);

  迭代语句;

}

### do-while循环

初始化语句;

do {

  循环体语句;

  迭代语句;

} while (循环条件);

### 死循环

for(;;) {
   System.*out*.println("Hello World");
 }

// 经典做法

while(true) {
   System.*out*.println("Hello World");
 }



do {
   System.*out*.println("Hello World");
 } while (true);

## 跳转关键字：break、continue

break   :  跳出并结束当前所在循环的执行。
continue:  用于跳出当前循环的当次执行，进入下一次循环。
##### 注意事项：
break : 只能用于结束所在循环, 或者结束所在switch分支的执行。
continue : 只能在循环中进行使用。